'use strict';


const Container = require('./Container');
const ContainerConfig = require('./ContainerConfig');
const DependencyNames = require('./DependencyNames');
const Rule = require('./DependencyRule');


const containerConfigs = {
  app: ContainerConfig.getFromFile()
};


let injectors = (function() {
  let injectors = {};
  for (let name in containerConfigs) {
    injectors[name] = new Injector(new Container(containerConfigs[name]));
  }
  return injectors;
})();


function Injector(container) {

  return {
    inject: inject
  };


  function inject(type, ruleList) {
    let rules = [];
    let ruleValues = [];
    normalizeRuleList();
    getRules();
    return _getModule(type);


    function normalizeRuleList() {
      ruleList = ruleList || [];
      if (ruleList.constructor === Array) return;
      let transformedRuleList = [];
      for (let str in ruleList) {
        transformedRuleList.push([str, ruleList[str]]);
      }
      ruleList = transformedRuleList;
    }

    function getRules() {
      rules = ruleList.map(function(r) {
        let ruleStr = r[0] + (r[0].endsWith('?') ? r[1] : '');
        return {
          rule: new Rule(ruleStr),
          ruleStr: ruleStr,
          hasImplementation: !r[0].endsWith('?'),
          implementation: r[1]
        };
      });

      rules.push({
        rule: new Rule('**'),
        ruleStr: '**',
        hasImplementation: false,
        implementation: {}
      });
    }

    function _getModule(ruleString) {
      let rule = new Rule(ruleString);
      let type = (function() {
        let parts = ruleString.split('?');
        return parts[0];
      })();
      let moduleIndex = container.getMatch(type, rule, []);
      if (moduleIndex < 0) {
        throw new Error(`Can not get module of type ${type}`);
      }
      let module = container.require(type, moduleIndex);
      let dependencyTypes = container.getDependencies(type, moduleIndex);
      let dependencies = dependencyTypes.map(function(type) {
        return _inject(type, rules, []);
      });
      let instance = module.class.apply(null, dependencies);
      if (typeof instance === 'function' || typeof instance === 'object') {
        instance.$dependencies = {};
        for (let i = 0; i < dependencyTypes.length; i++) {
          let type = dependencyTypes[i];
          instance.$dependencies[type] = dependencies[i];
        }
      }
      return instance;
    }
  }

  function _inject(type, rules, dependencyChain) {

    for (let i = 0; i < rules.length; i++) {
      let rule = rules[i];
      if (rule.hasImplementation) {
        if (container.doesRuleMatch(type, rule.rule, dependencyChain)) {
          return rule.implementation;
        }
      }
      else {
        let moduleIndex = container.getMatch(type, rule.rule, dependencyChain);
        if (moduleIndex >= 0) {
          let module = container.require(type, moduleIndex);
          let dependencyTypes = container.getDependencies(type, moduleIndex);
          let extendedChain = dependencyChain.concat([{
            type: type,
            descriptors: container.getDescriptors(type, moduleIndex)
          }]);
          let dependencies = dependencyTypes.map(function(type) {
            return _inject(type, rules, extendedChain);
          });
          let instance = module.class.apply(null, dependencies);
          if (typeof instance === 'function' || typeof instance === 'object') {
            instance.$dependencies = {};
            for (let i = 0; i < dependencyTypes.length; i++) {
              let type = dependencyTypes[i];
              instance.$dependencies[type] = dependencies[i];
            }
          }
          return instance;
        }
      }
    }
    throw new Error(`No matching rule for type ${type} in dependency chain`);
  }
}

module.exports = {
  create: function(config) {
    let containerConfig = ContainerConfig.createFromPaths(config);
    //console.log(modulePaths, containerConfig);
    return new Injector(new Container(containerConfig));
  },
  inject: function (injector, path, injectedDependencies) {
    return injectors[injector].inject(path, injectedDependencies);
  }
};