const rek = require('rekuire');
const expect    = require("chai").expect;
const sinon = require('sinon');
const Rule = rek('DependencyRule');

/*

 "Transactionable": [
 {
 "descriptors": {}
 }
 ],


 */

let chain = [
  {
    type: 'A',
    descriptors: {}
  },
  {
    type: 'B',
    descriptors: {}
  }
];

let descriptorSets = [
  {
    a: 1,
    b: 1
  },
  {
    a: 2,
    b: 2
  }
];



describe('DependencyRule', function() {

  it('gets a match when chain length is 0 and there is no query', function() {
    let rule = new Rule('A');
    let index = rule.getMatch([], 'A', descriptorSets);
    expect(index).to.equal(0);
  });

  it('gets a match when chain length is 0 and there is a matching query', function() {
    let rule = new Rule('A?a=2');
    let index = rule.getMatch([], 'A', descriptorSets);
    expect(index).to.equal(1);
  });

  it('does not get a match when chain length is 0 and there is no matching query', function() {
    let rule = new Rule('A?a=3');
    let index = rule.getMatch([], 'A', descriptorSets);
    expect(index).to.equal(-1);
  });


  it('1', function() {
    let rule = new Rule('A/B');
    let chain = [{
      type: 'A',
      descriptors: {}
    }]
    let index = rule.getMatch(chain, 'B', descriptorSets);
    expect(index).to.equal(0);
  });

  it('2', function() {
    let rule = new Rule('A/**/B');
    let chain = [{
      type: 'A',
      descriptors: {}
    }]
    let index = rule.getMatch(chain, 'B', descriptorSets);
    expect(index).to.equal(-1);
  });

  it('3', function() {
    let rule = new Rule('A/**/D');
    let chain = [
      {
        type: 'A',
        descriptors: {}
      },
      {
        type: 'B',
        descriptors: {}
      },
      {
        type: 'C',
        descriptors: {}
      }
    ];
    let index = rule.getMatch(chain, 'D', descriptorSets);
    expect(index).to.equal(0);
  });

  it('4', function() {
    let rule = new Rule('A/**/D');
    let chain = [
      {
        type: 'A',
        descriptors: {}
      },
      {
        type: 'B',
        descriptors: {}
      },
      {
        type: 'C',
        descriptors: {}
      },
      {
        type: 'D',
        descriptors: {}
      }
    ];
    let index = rule.getMatch(chain, 'D', descriptorSets);
    expect(index).to.equal(-1);
  });

  it('5', function() {
    let rule = new Rule('A/**/D/E');
    let chain = [
      {
        type: 'A',
        descriptors: {}
      },
      {
        type: 'B',
        descriptors: {}
      },
      {
        type: 'C',
        descriptors: {}
      },
      {
        type: 'D',
        descriptors: {}
      }
    ];
    let index = rule.getMatch(chain, 'E', descriptorSets);
    expect(index).to.equal(0);
  });

  it('6', function() {
    let rule = new Rule('**/C/D/**/G');
    let chain = [
      {
        type: 'A',
        descriptors: {}
      },
      {
        type: 'B',
        descriptors: {}
      },
      {
        type: 'C',
        descriptors: {}
      },
      {
        type: 'D',
        descriptors: {}
      },
      {
        type: 'E',
        descriptors: {}
      },
      {
        type: 'F',
        descriptors: {}
      }
    ];
    let index = rule.getMatch(chain, 'G', descriptorSets);
    expect(index).to.equal(0);
  });

  it('7', function() {
    let rule = new Rule('**?a=1/C/D?a=2/**/G');
    let chain = [
      {
        type: 'A',
        descriptors: {
          a: 1
        }
      },
      {
        type: 'B',
        descriptors: {
          a:1
        }
      },
      {
        type: 'C',
        descriptors: {}
      },
      {
        type: 'D',
        descriptors: {
          a:2
        }
      },
      {
        type: 'E',
        descriptors: {}
      },
      {
        type: 'F',
        descriptors: {}
      }
    ];
    let index = rule.getMatch(chain, 'G', descriptorSets);
    expect(index).to.equal(0);
  });

  it('8', function() {
    let rule = new Rule('**?a=1/C/D?a=2/**/G');
    let chain = [
      {
        type: 'A',
        descriptors: {
          a: 1
        }
      },
      {
        type: 'B',
        descriptors: {
          a:2
        }
      },
      {
        type: 'C',
        descriptors: {}
      },
      {
        type: 'D',
        descriptors: {
          a:2
        }
      },
      {
        type: 'E',
        descriptors: {}
      },
      {
        type: 'F',
        descriptors: {}
      }
    ];
    let index = rule.getMatch(chain, 'G', descriptorSets);
    expect(index).to.equal(-1);
  });

});