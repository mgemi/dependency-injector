
module.exports.$diConfig = {
  type: 'Car',
  descriptors: {
    model: 'camry',
    manufacturer: 'toyota'
  }
};

module.exports.class = function() {
  return {
    getName: () => 'Toyota Camry'
  };
};