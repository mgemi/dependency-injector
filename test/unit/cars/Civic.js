
module.exports.$diConfig = {
  type: 'Car',
  descriptors: {
    model: 'civic',
    manufacturer: 'honda'
  }
};

module.exports.class = function() {
  return {
    getName: () => 'Honda Civic'
  };
};