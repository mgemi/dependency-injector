
module.exports.$diConfig = {
  type: 'Car',
  descriptors: {
    model: 'corolla',
    manufacturer: 'toyota'
  }
};

module.exports.class = function() {
  return {
    getName: () => 'Toyota Corolla'
  };
};