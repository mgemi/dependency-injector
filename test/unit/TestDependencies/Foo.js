module.exports.$diConfig = {
  type: 'Foo'
};

module.exports.class = function(bar) {
  return {
    foo: function() {
      return `Foo, ${bar.bar}`;
    }
  };
};