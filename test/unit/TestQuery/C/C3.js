module.exports.$diConfig = {
  type: 'Ac',
  descriptors: {
    value: 3,
    parentDir: 'C'
  }

};

module.exports.class = function(aca, acb, acc) {
  return {
    a: aca,
    b: acb,
    c: acc,
    value: 'ac3'
  };
};