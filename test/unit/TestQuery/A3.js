module.exports.$diConfig = {
  type: 'A',
  descriptors: {
    value: 3
  }

};

module.exports.class = function(aa, ab, ac) {
  return {
    a: aa,
    b: ab,
    c: ac,
    value: 'a3'
  };
};