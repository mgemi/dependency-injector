module.exports.$diConfig = {
  type: 'Ab',
  descriptors: {
    value: 3,
    parentDir: 'B'
  }

};

module.exports.class = function(aba, abb, abc) {
  return {
    a: aba,
    b: abb,
    c: abc,
    value: 'ab3'
  };
};