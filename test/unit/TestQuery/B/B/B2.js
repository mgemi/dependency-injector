module.exports.$diConfig = {
  type: 'Abb',
  descriptors: {
    value: 2,
    parentDir: 'B'
  }

};

module.exports.class = function() {
  return {
    value: 'abb2'
  };
};