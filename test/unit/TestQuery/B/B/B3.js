module.exports.$diConfig = {
  type: 'Abb',
  descriptors: {
    value: 3,
    parentDir: 'B'
  }

};

module.exports.class = function() {
  return {
    value: 'abb3'
  };
};