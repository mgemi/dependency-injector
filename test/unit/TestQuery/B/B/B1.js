module.exports.$diConfig = {
  type: 'Abb',
  descriptors: {
    value: 1,
    parentDir: 'B'
  }

};

module.exports.class = function() {
  return {
    value: 'abb1'
  };
};