module.exports.$diConfig = {
  type: 'A',
  descriptors: {
    value: 2
  }

};

module.exports.class = function(aa, ab, ac) {
  return {
    a: aa,
    b: ab,
    c: ac,
    value: 'a2'
  };
};