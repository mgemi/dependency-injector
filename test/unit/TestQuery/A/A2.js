module.exports.$diConfig = {
  type: 'Aa',
  descriptors: {
    value: 2,
    parentDir: 'A'
  }

};

module.exports.class = function(aaa, aab, aac) {
  return {
    a: aaa,
    b: aab,
    c: aac,
    value: 'aa2'
  };
};