module.exports.$diConfig = {
  type: 'Aac',
  descriptors: {
    value: 3,
    parentDir: 'C'
  }

};

module.exports.class = function() {
  return {
    value: 'aac3'
  };
};