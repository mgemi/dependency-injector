module.exports.$diConfig = {
  type: 'Aac',
  descriptors: {
    value: 2,
    parentDir: 'C'
  }

};

module.exports.class = function() {
  return {
    value: 'aac2'
  };
};