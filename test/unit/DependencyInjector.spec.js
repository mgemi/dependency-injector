const rek = require('rekuire');
const expect    = require("chai").expect;
const sinon = require('sinon');
const Injector = rek('index.js');
const path = require('path');


const injector = Injector.create({
  require: require,
  paths: [
    './TestDependencies/A',
    './TestDependencies/B',
    './TestDependencies/Bar',
    './TestDependencies/C',
    './TestDependencies/D',
    './TestDependencies/Foo',
    './TestDependencies/NormalModule',
    './TestDependencies/Qux',
    './cars/Camry',
    './cars/Civic',
    './cars/Corolla'
  ]
});


const injectorDeep = (function() {
  let paths = getPaths('./TestQuery/', 'A', 1).map((path) => './' + path);
  return Injector.create({
    require: require,
    paths: paths
  });


  function getPaths(parent, file, level) {
    let files = [1,2,3].map((num) => path.join(parent, file + num));
    if (level < 3) {
      ['A', 'B', 'C'].map(function (dir) {
        files = files.concat(getPaths(path.join(parent, dir), dir, level + 1));
      });
    }
    return files;
  }
})();


describe('DependencyInjector', function() {

  it('lets you require a module with no dependencies', function() {
    let c = injector.inject('C');
    expect(c).to.equal('C');
  });

  it('lets you require a module with dependencies', function() {

    let b = injector.inject('B', {
      c: 'C',
      d: 'D'
    });
    expect(b).to.equal('C, D');
  });

  it("throws an error if all the required dependencies aren't passed", function() {
    let injector = Injector.create({
      require: require,
      paths: [
        './TestDependencies/B'
      ]
    });

    expectError('No matching rule for type D in dependency chain', function() {
      injector.inject('B', {
        c: 'C'
      });
    });
  });

  it('Lets you override a dependency added to a container', function() {
    let b = injector.inject('B', {
      d: 'DD'
    });
    expect(b).to.equal('C, DD');
  });


  it('Lets you require modules that require other modules', function() {
    let a = injector.inject('A');
    expect(a).to.equal('A, C, D');
  });

  it('Lets you stub module dependencies', function() {
    let foo = injector.inject('Foo');
    foo.$dependencies.Bar.bar = 'bar!';
    expect(foo.foo()).to.equal('Foo, bar!');
  });


  it('lets you query a module', function() {
    let toyotaCamry = injector.inject('Car?model=camry&manufacturer=toyota');
    let toyotaCorolla = injector.inject('Car?model=corolla&manufacturer=toyota');
    let hondaCivic = injector.inject('Car?model=civic&manufacturer=honda');
    expect(toyotaCamry.getName()).to.equal('Toyota Camry');
    expect(toyotaCorolla.getName()).to.equal('Toyota Corolla');
    expect(hondaCivic.getName()).to.equal('Honda Civic');
  });

  it('a1', function() {
    let a = injectorDeep.inject('A?value=3');
    expect(a.value).to.equal('a3');
  });

  it('a2', function() {
    let a = injectorDeep.inject('A', {
      '**?': 'value=2'
    });
    expect(a.b.value).to.equal('ab2');
    expect(a.c.value).to.equal('ac2');
    expect(a.b.c.value).to.equal('abc2');
  });

  it('a3', function() {
    let a = injectorDeep.inject('A', [
      ['aa/aaa?', 'value=1'],
      ['aa/*?', 'value=3'],
      ['**?', 'value=2']
    ]);
    expect(a.a.a.value).to.equal('aaa1');
    expect(a.a.b.value).to.equal('aab3');
    expect(a.a.c.value).to.equal('aac3');
    expect(a.b.value).to.equal('ab2');
    expect(a.c.value).to.equal('ac2');
    expect(a.b.c.value).to.equal('abc2');
  });

  it('a4', function() {
    let a = injectorDeep.inject('A', [
      ['*/aaa?', 'value=2'],
      ['**?', 'value=3']
    ]);
    expect(a.a.a.value).to.equal('aaa2');
    expect(a.b.value).to.equal('ab3');
  });

  it('a4', function() {
    let a = injectorDeep.inject('A', [
      ['aa', 'AA'],
      ['ab/*', 'AB*'],
      ['**?', 'value=3']
    ]);
    expect(a.a).to.equal('AA');
    expect(a.a.a).to.be.undefined;
    expect(a.b.a).to.equal('AB*');
    expect(a.b.b).to.equal('AB*');
    expect(a.c.a.value).to.equal('aca3');
  });

  it('a5', function() {
    let a = injectorDeep.inject('A', [
      ['**?parentDir=B/*', 'B'],
      ['**?parentDir=C/*', 'C']
    ]);
    expect(a.b.a).to.equal('B');
    expect(a.b.b).to.equal('B');
    expect(a.c.a).to.equal('C');
    expect(a.c.b).to.equal('C');
  });

  it('a6', function() {
    let a = injectorDeep.inject('A', [
      ['ab?', 'value=3'],
      ['*?value=3/*?', 'value=3'],
      ['**?', 'value=2']
    ]);
    expect(a.b.value).to.equal('ab3');
    expect(a.b.a.value).to.equal('aba3');
    expect(a.c.value).to.equal('ac2');
    expect(a.c.a.value).to.equal('aca2');
  });

  /*
  it('a7', function() {
    let a = injectorDeep.inject('A', {
      '*?parentDir=A&value=2/*?': 'value=2'
    });
    expect(a.b.value).to.equal('ab3');
    expect(a.b.a.value).to.equal('aba3');
    expect(a.c.value).to.equal('ac2');
    expect(a.c.a.value).to.equal('aca2');
  });
  */
});

function expectError(message, errorFunction) {
  expect(errorFunction).to.throw(message);
}