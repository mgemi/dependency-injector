

module.exports.$diConfig = {
  type: 'Transactionable',
  isSingleton: false
};

module.exports.class = function(Q, eventManager, pusher) {

  let updates = [];
  let transactionStarted = false;

  return {
    addUpdate: addUpdate,
    begin: begin,
    commit: commit,
    rollback: rollback
  };

  function onCommit(callback) {
    eventManager.on('commit', callback);
  }

  function onRollback(callback) {
    eventManager.on('rollback', callback);
  }

  function addUpdate(type, data) {
    updates.push({
      type: type,
      data: data
    });
    if (!transactionStarted) {
      return commit();
    }
  }

  function begin() {
    transactionStarted = true;
  }

  function commit() {
    let deferred = Q.defer();

    pusher.push(updates)
      .then(success)
      .catch(error)
      .finally(always);

    function success(result) {
      updates = [];
      deferred.resolve(result);
      eventManager.trigger('commit');
    }

    function error(error) {
      deferred.reject(error);
    }

    function always() {
      transactionStarted = false;
    }

    return deferred.promise;
  }

  function rollback() {
    updates = [];
    transactionStarted = false;
    eventManager.trigger('rollback');
  }
};