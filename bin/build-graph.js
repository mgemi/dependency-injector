#!/usr/bin/env node
const rek = require('rekuire');
const fs = require('fs');
const path = require("path");
const recursive = require('recursive-readdir');
const commandLineArgs = require('command-line-args')
const ContainerConfig = require('../ContainerConfig');
const Q = require('q');

let options = (function() {
  const optionDefinitions = [
    { name: 'test', alias: 't', type: Boolean, defaultValue: false }
  ];
  return commandLineArgs(optionDefinitions);
})();

let parentDir = (function() {
  return options.test ? path.join(__dirname, '..') : path.join(__dirname, '..', '..', '..')
})();


function getContainerDirs() {
  return dirs = [parentDir].concat(getContainerExtensionDirs());
}

function getContainerExtensionDirs() {
  let pkg = getPackageFile();
  let containerExtensions = pkg.containerExtensions || [];
  return containerExtensions.map(function(extension) {
    return path.join(parentDir, 'node_modules', extension);
  });
}

function getPackageFile() {
  let pkg = {};
  try {
    pkg = require(path.join(parentDir, 'package.json'));
  }
  catch(e) {}
  return pkg;
}

function getPath(file) {
  if (options.test) {
    return file;
  }
  //return path.relative(parentDir, file);
  return file;
}


(function() {

  let containerDirs = getContainerDirs();
  let pathPromises = containerDirs.map(function(dir) {
    return getModulePaths(dir);
  });
  Q.allSettled(pathPromises).then(function(results) {
    let paths = [];
    results.forEach(function(result) {
      paths = paths.concat(result.value);
    });
    createContainerFile(paths);
  });
})();


function createContainerFile(paths) {
  let container = ContainerConfig.createFromPaths({
    paths: paths
  });
  let fileName = path.join(parentDir, 'diContainer.json');
  fs.writeFileSync(fileName, JSON.stringify(container, null, 2));
}

function getModulePaths(parentDir) {

  let deferred = Q.defer();

  recursive(parentDir, [ignoreFunc], function (err, files) {
    if (err) {
      deferred.resolve([]);
    }
    else {
      let paths = files.map(function (file) {
        return getPath(file);
      });
      deferred.resolve(paths);
    }
  });

  return deferred.promise;
}

function ignoreFunc(file, stats) {
  let isDir = stats.isDirectory();
  let isNodeModulesDir = isDir && path.basename(file) === "node_modules";
  let isHiddenFile = path.basename(file).substr(0,1) === ".";
  let isJsFile = path.extname(file) === '.js';

  return isNodeModulesDir || isHiddenFile || (!isJsFile && !isDir);
}