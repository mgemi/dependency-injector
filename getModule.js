

module.exports.fromCreateContainerConfig = function(config, path) {
  let module = {};
  try {
    if (config.require) {
      module = config.require(path);
    }
    else {
      module = require(path);
    }
  }
  catch(e) {}
  return module;
};



module.exports.fromModuleConfig = function(config) {
  let module = {};
  try {
    if (config.require) {
      module = config.require(config.path);
    }
    else {
      module = require(config.path);
    }
  }
  catch(e) {}
  return module;
};