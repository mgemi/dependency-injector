const DependencyNames = require('./DependencyNames');


let NullSegment =  {
  isWildCard: () => false,
  isDeepWildCard: () => false,
  isMatch: () => false,
  isNull: () => true
};


function Segment(segmentString) {

  let moduleName = '';
  let query = {};

  getSegmentParts();

  return {
    isWildCard: isWildCard,
    isDeepWildCard: isDeepWildCard,
    isMatch: isMatch,
    isNull: () => false
  };


  function getSegmentParts() {
    let parts = segmentString.split('?');
    moduleName = DependencyNames.normalizeName(parts[0]);
    let queryString = parts.length === 2 ? parts[1] : '';
    setQuery(queryString);
  }

  function setQuery(queryString) {
    let queryParams = queryString.split('&');
    queryParams.forEach(function(param) {
      let parts = param.split('=');
      let key = parts[0];
      let value = parts[1];
      query[key] = value;
    });
  }

  function isWildCard() {
    return moduleName === '*' || moduleName === '**';
  }

  function isDeepWildCard() {
    return moduleName === '**';
  }

  function isMatch(module) {
    if (!isWildCard() && module.type !== moduleName) {
      return false;
    }

    for (let key in query) {
      let value = query[key];
      if (value != module.descriptors[key]) {
        return false;
      }
    }

    return true;
  }
}


function Rule(ruleString) {

  let isValid = isRuleValid();
  let segments = isValid ? getSegments() : [];


  return {
    getMatch: getMatch
  };


  function getMatch(chain, type, descriptorSets) {

    let segNum = 0;
    let chainNum = 0;
    for (; segNum < chain.length && chainNum < chain.length;) {
      let segment = getSegment(segNum);

      let isMatch = segment.isMatch(chain[chainNum]);
      if (!isMatch) {
        return -1;
      }

      let lastChain = chainNum === chain.length - 1;
      if (!segment.isDeepWildCard() || (!lastChain && getSegment(segNum + 1).isMatch(chain[chainNum + 1])) ||
        (lastChain && !isLastSeg(segNum))) {
        segNum++;
      }

      chainNum++;
    }

    if (!isLastSeg(segNum)) return -1;
    let segment = getSegment(segNum);

    for (let i = 0; i < descriptorSets.length; i++) {
      let isMatch = segment.isMatch({
        type: type,
        descriptors: descriptorSets[i]
      });

      if (isMatch) {
        return i;
      }
    }
    return -1;
  }

  function isLastSeg(index) {
    return index === segments.length - 1;
  }

  function getSegment(index) {
    return segments.length > index ? segments[index] : NullSegment;
  }


  function getSegments() {
    return ruleString.split('/').map(function(segmentString) {
      return new Segment(segmentString);
    });
  }

  function isRuleValid() {
    let name = '([a-zA-Z][a-zA-Z0-9]*)';
    let value = '([a-zA-Z0-9])*';
    let module = `(${name}|\\*|\\*\\*)`;
    let queryParam = `(${name}=${value})`;
    let query = `(${queryParam}(&${queryParam})*)`;
    let queryString = `(\\?${query})`;
    let segment = `(${module}${queryString}?)`;
    let rule = `^${segment}(/${segment})*(\\?)?$`;
    let re = new RegExp(rule);
    return re.test(ruleString);
  }
}


module.exports = Rule;