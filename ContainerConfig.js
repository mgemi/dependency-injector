
const DependencyNames = require('./DependencyNames');
const getModule = require('./getModule');
const path = require("path");

module.exports.createFromPaths = function(config) {

  let container = {};

  config.paths.forEach(function(path) {
    let mod = getModule.fromCreateContainerConfig(config, path);
    if (!isModuleValid(mod)) return;
    let moduleConfig = getModuleConfig(mod, path, config);
    let types =  normalizeModuleTypes(mod.$diConfig.type);
    types.forEach(function(type) {
      let normalizedType = DependencyNames.normalizeName(type);
      if (container[normalizedType] === undefined) container[normalizedType] = [];
      container[normalizedType].push(moduleConfig);
    });
  });

  return container;
};

function getModuleConfig(module, path, config) {
  let moduleConfig = {
    path: path,
    isSingleton: !!module.$diConfig.isSingleton,
    dependencies: DependencyNames.getFromModule(module.class),
    descriptors: module.$diConfig.descriptors || {}
  };
  if (config.require) {
    moduleConfig.require = config.require;
  }
  return moduleConfig;
}


function normalizeModuleTypes(types) {
  if (typeof types === 'undefined') {
    return [];
  }
  if (typeof types === 'string') {
    return [types];
  }
  return types;
}


function isModuleValid(module) {
  return !!module.$diConfig && !!module.class;
}

function requireFromParent(relativePath) {
  return require(path.join(__dirname, '..', '..', relativePath));
  //return require('./' + relativePath);
}


module.exports.getFromFile = function() {
  let containerConfig = {};
  try {
    containerConfig = requireFromParent('diContainer.json');
  }
  catch(e) {

  }
  return containerConfig;
};
