
const getModule = require('./getModule');

module.exports = function(containerConfig) {
  return {
    getMatch: function(type, rule, dependencyChain) {
      let typeConfigs = containerConfig[type] || [];
      let descriptorSets = typeConfigs.map(function(config) {
        return config.descriptors;
      });
      return rule.getMatch(dependencyChain, type, descriptorSets);
    },
    doesRuleMatch: function(type, rule, dependencyChain) {
      let matchIndex = rule.getMatch(dependencyChain, type, [{}]);
      return matchIndex === 0;
    },
    require: function (type, index) {
      this.assertHasModule(type, index);
      index = typeof index === 'undefined' ? 0 : index;
      return getModule.fromModuleConfig(containerConfig[type][index]);
    },
    getDependencies: function (type, index) {
      this.assertHasModule(type, index);
      index = typeof index === 'undefined' ? 0 : index;
      return containerConfig[type][index].dependencies;
    },
    getDescriptors: function (type, index) {
      this.assertHasModule(type, index);
      index = typeof index === 'undefined' ? 0 : index;
      return containerConfig[type][index].descriptors;
    },
    assertHasModule: function(type, index) {
      index = typeof index === 'undefined' ? 0 : index;
      if (containerConfig[type] === undefined) {
        throw new Error(`Could not find module of type ${type} in container`);
      }
      if (containerConfig[type][index] === undefined) {
        throw new Error(`Could not find module of type ${type} in container at index ${index}`);
      }
    },
    hasModule: function (type) {
      return type in containerConfig;
    }
  }
};