
module.exports.getFromModule = function(func) {
  let moduleNames = getModuleNames(func);
  return moduleNames.map(function(name) {
    return normalizeName(name);
  });
};

module.exports.normalizeName = normalizeName;


function normalizeName(name) {
  return name.charAt(0).toUpperCase() + name.slice(1);
};

// getParamNames from http://stackoverflow.com/questions/1007981/how-to-get-function-parameter-names-values-dynamically-from-javascript

var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
var ARGUMENT_NAMES = /([^\s,]+)/g;


function getModuleNames(func) {
  var fnStr = func.toString().replace(STRIP_COMMENTS, '');
  var result = fnStr.slice(fnStr.indexOf('(')+1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
  if(result === null)
    result = [];
  return result;
}